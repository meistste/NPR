#include "..\..\include\Editor\MenuBar.h"
#include "../../libs/imgui/imgui.h"
#include <windows.h>
#include <string.h>
#include <iostream>
#include "../../include/Scene.h"
#include <filesystem>

namespace Editor
{
	void FileMenu()
	{
		if (ImGui::MenuItem("Open Scene"))
		{
			OPENFILENAME ofn;
			char fileName[MAX_PATH] = "";
			ZeroMemory(&ofn, sizeof(ofn));

			ofn.lStructSize = sizeof(OPENFILENAME);
			ofn.hwndOwner = NULL;
			ofn.lpstrFilter = "All Files (*.*)\0*.*\0";
			ofn.lpstrFile = fileName;
			ofn.nMaxFile = MAX_PATH;
			ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
			ofn.lpstrDefExt = "";

			std::string fileNameStr;

			if (!GetOpenFileName(&ofn))
			{
				printf("No scene selected\n");
			}
			else
			{
				fileNameStr = ofn.lpstrFile;
				loadScene(fileNameStr);
			}
		}
	}

	void DrawMenuBar()
	{
		if (ImGui::BeginMainMenuBar())
		{
			if (ImGui::BeginMenu("File"))
			{
				FileMenu();
				ImGui::EndMenu();
			}
			ImGui::EndMainMenuBar();
		}
	}
}